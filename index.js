#!/usr/bin/env node
// Primary focus is: Default, French, German

const fs = require('fs'),
    readline = require('readline'),
    async = require('async'),
    inquirer = require('inquirer')

var prompts = [
    {
        type: 'input',
        name: 'localesToUse',
        message: 'Which locales should be used? (comma separated list)',
        default: 'default'
    },
    {
        type: 'input',
        name: 'basePath',
        message: 'Base path to prefix the individual cartridge paths',
        default: './'
    },
    {
        type: 'input',
        name: 'cartridgePath',
        message: 'Cartridge path? (cartridges separated by colon : )',
        default: ''
    },
    {
        type: 'confirm',
        name: 'showMissingOnly',
        message: 'Show only missing language entries?',
    }
];
inquirer.prompt(prompts).then(function (responses) {
    console.log("here");
    console.log(responses);
})

/**
 * Should the generated spreadsheet only show missing entries?
 */
const showMissingOnly = true

/**
 * Which Locales should be used?
 */
const localesToUse = ['default', 'de', 'fr']

/**
 * Cartridge Path - full path to any catridges that need reviewed separated by a colon
 * The order here is important because we will loop across these files in order
 */
const cartridgePath = "projects/mfsg/cartridges/app_storefront_base:projects/puma/cartridges/app_puma_core"

const cartridgesDirty = cartridgePath.split(":")
const cartridges = cartridgesDirty.map((value, index) => {
    return value.trim()
})

/**
 * Main Loop
 * This block will loop across the cartridges and analyze any locale files found
 * all results across files and cartridges will be compiled in the localeSections array
 */
// let localeSections = {}

// for (let key in cartridges) {
//     if (cartridges[key]) {
//         let cartridgePath = cartridges[key]

//         let cartLocaleFiles = fs.readdirSync(cartridgePath + "/cartridge/templates/resources")

//         for (let key in cartLocaleFiles) {
//             if (cartLocaleFiles[key]) {
//                 analyzeFile(cartridgePath + "/cartridge/templates/resources/" + cartLocaleFiles[key])
//             }
//         }
//     }
// }

/**
 * Output results to JSON and CSV
 */
// outputResults(localeSections)

/**
 * Analyze File
 * Read the contents of the supplied file and parse all locale entries and add to the 
 * localeSections array. Results will stack to allow overrides between files. 
 * @param {String} value 
 */
function analyzeFile(value) {
    console.log("Analyzing: " + value + "...")

    let localeFile = {
        "file": value,
        "fileName": value.split("/").pop(),
        "name": value.split("/").pop().replace(".properties", ""),
        "localeSection": ""
    }

    let localeParts = localeFile.name.split("_")
    delete localeParts[0]

    localeFile.locale = localeParts.join("_").substr(1)

    /**
     * If no locale was found, this must be a default file
     */
    if (localeFile.locale === "") {
        localeFile.locale = "default"
    }

    /**
     * Find the locale section by removing the locale from the filename
     */
    localeFile.localeSection = localeFile.name.replace("_" + localeFile.locale, "")

    /**
     * Do some prep work / initialization to the localeSections array
     */
    if (!localeSections[localeFile.localeSection]) {
        localeSections[localeFile.localeSection] = {
            files: [],
            locales: [],
            entries: {}
        }
    }

    localeSections[localeFile.localeSection].files.push(localeFile.file)

    /**
     * Add Locale if applicable
     */
    let saveLocale = false
    for (let key in localesToUse) {
        if (localesToUse[key]) {
            if (localeFile.locale.indexOf(localesToUse[key]) > -1) saveLocale = true
        }
    }

    if (saveLocale && localeSections[localeFile.localeSection].locales.indexOf(localeFile.locale) < 0) {
        localeSections[localeFile.localeSection].locales.push(localeFile.locale)
    }

    /**
     * Read Locale File and parse the content
     */
    let content = fs.readFileSync(localeFile.file, 'UTF8')
    let lines = content.split("\n")

    for (let key in lines) {
        if (lines[key]) {
            let line = lines[key].trim()

            /**
             * Comments
             */
            if (line.substr(0, 1) === "#") {
                continue
            }

            /**
             * Blank Lines
             */
            if (line === "") {
                continue
            }

            let parts = line.split("=")
            let entryKey = parts[0] + ""
            let entryValue = parts[1] + ""

            /**
             * Important so that trailing spaces don't create multiple keys
             */
            entryKey = entryKey.trim()
            entryValue = entryValue.trim()

            /**
             * Initialize
             */
            if (!localeSections[localeFile.localeSection].entries[entryKey]) {
                localeSections[localeFile.localeSection].entries[entryKey] = {}
            }

            /**
             * Set
             */
            localeSections[localeFile.localeSection].entries[entryKey][localeFile.locale] = entryValue
        }
    }
}

/**
 * Output results to JSON and CSV
 * 
 * @param {Array} localeSections 
 */
function outputResults(localeSections) {
    let output = ""

    fs.writeFile("output.json", JSON.stringify(localeSections, null, "\t"))
    let localeSectionsKeys = Object.keys(localeSections)

    for (let key in localeSectionsKeys) {
        if (localeSectionsKeys[key]) {
            let localeSection = localeSections[localeSectionsKeys[key]]

            output += localeSectionsKeys[key] + "\n"

            for (let key in localeSection.files) {
                if (localeSection.files[key]) {
                    let file = localeSection.files[key]
                    output += file + "\n"
                }
            }

            output += "Key,"
            for (let key in localeSection.locales) {
                if (localeSection.locales[key]) {
                    let locale = localeSection.locales[key]
                    output += locale + ","
                }
            }
            output += "\n"

            for (let entryKey in localeSection.entries) {
                if (localeSection.entries[entryKey]) {
                    let entry = localeSection.entries[entryKey]

                    if (!entry["default"] || entry["default"] === "" || entry["default"].length < 1) {
                        console.log("Skipping entry where default value is not present ... " + localeSectionsKeys[key] + " - " + entryKey)
                        continue
                    }

                    output += entryKey + ","

                    for (let key in localeSection.locales) {
                        if (localeSection.locales[key]) {
                            let locale = localeSection.locales[key]

                            if (entry[locale]) {
                                if (showMissingOnly && locale !== "default") {
                                    output += ","
                                } else {
                                    output += "\"" + entry[locale].replace("\"", "\"\"") + "\","
                                }
                            } else {
                                output += "XXXXXXXXXX,"
                            }
                        }
                    }

                    output += "\n"
                }
            }

            output += "\n"
        }
    }

    fs.writeFile("output.csv", output)
}